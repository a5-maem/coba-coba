# Maem, a food delivery app

Maem is a web-based food delivery service that allows user to order food
from many restaurants and delivered straight to their homes with just a click
of a button.

###### Features

* Promo & Voucher
    * Shows promos and vouchers available to use in food orders. Promos and vouchers will automatically be updated to Restaurant List and applied to corresponding menus.

* Order item
    * User can order items on menus from restaurants available on Restaurant List page and make customizations to their items (ex: extra spicy, no sugar, etc). Proceeds to checkout and input delivery address and payment. The order will then be confirmed and user can view their order on Order Status.
 
* Order Status
    * User can view the status of their orders after confirmation (ex: Processing order, Order is on the way, Order arrived, Completed, dll). Order status changes based on distance of delivery address updates.

* Multi-order (WIP)
    * User can have multiple orders at once (parallel).

* History
    * Stored in User Profile, user can see their completed order history. Updates on History based on completed Order Status.

* Favorite
    * Stored in User Profile, user can see their favorite restaurants list ranked by the frequency of orders from corresponding restaurants.

###### Microservices
*_Eureka Registry Service_*
<br>Holds access to address of each microservices</br>

*_List Restoran_*
<br> Gets restaurant data in JSON format from model and shown in Restaurant List page</br>

*_Promo & Voucher_*
<br> Shows icon/notification automatically on each restaurant that has promo/voucher</br>

###### Design Pattern

* Observer Pattern
    * For *Multi-order*, *Status pemesanan*, *Favorite*, *Promo & Voucher*
* Decorator Pattern
    * For *Order Item*

###### Features Assignment
| Member | Features |
| --- | --- |
| Muhammad Azmie Alaudin | Order Status |
| Muhammad Irfan Amrullah | MainApplication, Promo & Voucher |
| Mutia Rahmatun Husna | History |
| Nabila Fathia Zahra | EurekaRegistryService, Order Item |
| Ryan Aulia Rachman | Favorite |


## Notes for developers
To start your own feature/service, create your own Spring Project (see EurekaRegistryService / MainApplication). Don't forget to add a README explaining your feature and a .gitignore (see the ones on outer directory). **Don't forget to implement TDD**

###### Starting a microservice
* Start a Spring project
    * https://spring.io/guides/gs/serving-web-content/#scratch
* Registering your microservice to EurekaRegistryService
     1. Configure these 3 files to register your service to the Eureka Server
     
        * **Your service app (ex: OrderItemServiceApp)**
        ``` java
        package maem;
        
        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;
        import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

        
       
        @SpringBootApplication      //Menandakan bahwa ini adalah aplikasi Spring
        @EnableEurekaClient         //Menandakan bahwa ini adalah Eureka Client
                                    //yg bisa dicari Eureka Server
        
        public class OrderItemServiceApp {
        
            public static void main(String[] args) {
                System.setProperty("spring.config.name", "application");    //bakal nyari application.yml
                SpringApplication.run(OrderItemServiceApp.class, args);
            }
        }

        ```
        * **build.gradle**
        <br>Tambahkan dependencies berikut</br>
        ``` java
        .
        .
        dependencies {
                classpath("org.springframework.boot:spring-boot-gradle-plugin:2.0.5.RELEASE")
                classpath("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:1.4.3.RELEASE")
        
            }
        .
        .
        ```
        ``` java
        .
        .
        dependencyManagement {
            imports {
                mavenBom 'org.springframework.cloud:spring-cloud-dependencies:Finchley.SR2'
            }
        }
        dependencies {
            //compile("org.springframework.cloud:spring-cloud-starter-netflix-eureka-server:1.4.3.RELEASE")
            compile("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:1.4.3.RELEASE")
            compile("org.springframework.boot:spring-boot-starter-web")
            compile("org.springframework.boot:spring-boot-starter-thymeleaf")
            compile("org.springframework.boot:spring-boot-devtools")
            testCompile("junit:junit")
        }
        ```
        * **application.yml** pada directory yg sejajar dengan file build.gradle
        ```yml
        # Spring properties
        spring:
          application:
            name: orderItemService
        
        # Discovery Server Access
        eureka:
          client:
            serviceUrl:
              defaultZone: http://localhost:1111/eureka/
        
        # HTTP Server
        server:
          port: xxxx   # HTTP (Tomcat) port -> port service Anda

        ```
     	###### List port service
     
     	| Service | Port |
     	| --- | --- |
     	| EurekaRegistryService | 1111 |
     	| OrderItemService | 2222 |
     	| PromoVoucher | ? |
     	| MainApplication | ? |
     	| Favorite | ? |
     	| History | 6666 |
     	| OrderStatus | ? |

     2. Run Eureka Server dengan menjalankan ```gradle bootRun``` di EurekaRegistryService, lalu Run app service Anda. Cek ```localhost:1111``` dan cek apakah service Anda sudah terdaftar.

**Kelompok A5**

* Muhammad Azmie Alaudin - 1606875945
* Muhammad Irfan Amrullah - 1706039585
* Mutia Rahmatun Husna - 1706039622
* Nabila Fathia Zahra - 1706022741
* Ryan Aulia Rachman - 1606918534 
