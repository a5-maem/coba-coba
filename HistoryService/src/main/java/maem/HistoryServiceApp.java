package maem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;



@SpringBootApplication      //Menandakan bahwa ini adalah aplikasi Spring
@EnableEurekaClient         //Menandakan bahwa ini adalah Eureka Client
                            //yg bisa dicari Eureka Server

public class HistoryServiceApp {

    public static void main(String[] args) {
        System.setProperty("spring.config.name", "application");    //bakal nyari application.yml
        SpringApplication.run(HistoryServiceApp.class, args);
    }
}
