package maem.controllers;

import maem.services.HistorySystem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;
import maem.services.Checkout;
import maem.services.MenuList;

@Controller

public class HistoryServiceController {

    private HistorySystem historySystem;

    @GetMapping("/history")
    public String showMenuList(@RequestParam(name = "name", required = false)
                                           String name, Model model) {
        historySystem.start();
        return "history";
    }


}
